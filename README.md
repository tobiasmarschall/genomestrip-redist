# README #

The purpose of this repository is to redistribute GenomeSTRIP, a tool developed by Bob Handsaker (and others), in order to make it easier to package it on [Bioconda](https://bioconda.github.io/).
Please refer to the [GenomeSTRiP homepage](http://software.broadinstitute.org/software/genomestrip/) and the [LICENSE file](https://bitbucket.org/tobiasmarschall/genomestrip-redist/downloads/LICENSE.txt) for more information.
